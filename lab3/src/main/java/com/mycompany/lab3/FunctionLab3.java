/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

import static com.mycompany.lab3.Lab3.table;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class FunctionLab3 {
    
    static boolean checkWinO(char table[][]) {
    for (int i = 0; i < 3; i++) {
        // Row or Column
        if ((table[i][0] == 'O' && table[i][1] == 'O' && table[i][2] == 'O') ||
            (table[0][i] == 'O' && table[1][i] == 'O' && table[2][i] == 'O')) {
            return true;
        }
    }
    // Diagonal
    if ((table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O') ||
        (table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O')) {
        return true;
    }

    return false;
}
    
    static boolean checkWinX(char table[][]) {
    for (int i = 0; i < 3; i++) {
        // Row or Column
        if ((table[i][0] == 'X' && table[i][1] == 'X' && table[i][2] == 'X') ||
            (table[0][i] == 'X' && table[1][i] == 'X' && table[2][i] == 'X')) {
            return true;
        }
    }
    // Diagonal
    if ((table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X') ||
        (table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X')) {
        return true;
    }

    return false;
}
    public static void turnO() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn O input row,column : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table[row-1][col-1] = 'O' ;
    }

    public static void turnX() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn O input row,column : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table[row-1][col-1] = 'X' ;
    }
    
    public static boolean draw(int count) {
        if (count == 9){ return true ;} else{ return false ;}
    }
    public static void printWelcome() {
        System.out.println("Welcome to OX game"); 
    }
    public static void printTable(char[][] table) {
        for (int i = 0 ; i < 3 ; i++){
            for (int j = 0 ; j < 3 ; j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }           
    }
}
