/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

import static com.mycompany.lab3.FunctionLab3.printWelcome;
import java.util.Scanner;

/**
 *
 * @author test1
 */
public class Lab3 {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}} ;
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int count = 0 ;
        
        printWelcome();
        FunctionLab3.printTable(table);
        while(true){
            FunctionLab3.turnO();
            FunctionLab3.printTable(table);
            if (FunctionLab3.checkWinO(table) == true){ System.out.println("O win!"); break; }
            count++ ;
            if (FunctionLab3.draw(count)) {System.out.println("Draw!"); break;}
            FunctionLab3.turnX();
            FunctionLab3.printTable(table);
            if (FunctionLab3.checkWinX(table) == true){ System.out.println("X win!"); break ;}
            count++ ;
            if (FunctionLab3.draw(count)) {System.out.println("Draw!"); break;}
            
        }

    }



}
