/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class testLab3 {
    
    public testLab3() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testOwinRow_1(){
        char[][] table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinRow_2(){
        char[][] table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinRow_3(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
        @Test
    public void testOwinCol_1(){
        char[][] table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinCol_2(){
        char[][] table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinCol_3(){
        char[][] table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinDiagonal_URDL(){
        char[][] table = {{'-','-','O'},{'-','O','-'},{'O','-','-'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testOwinDiagonal_ULDR(){
        char[][] table = {{'O','-','-'},{'-','O','-'},{'-','-','O'}} ;
        boolean result = FunctionLab3.checkWinO(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinRow_1(){
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinRow_2(){
        char[][] table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinRow_3(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
        @Test
    public void testXwinCol_1(){
        char[][] table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinCol_2(){
        char[][] table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinCol_3(){
        char[][] table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinDiagonal_URDL(){
        char[][] table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testXwinDiagonal_ULDR(){
        char[][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}} ;
        boolean result = FunctionLab3.checkWinX(table) ;
        assertEquals(true,result);
    }
    @Test
    public void testDraw1(){
        char[][] table = {{'O','O','X'},{'X','X','O'},{'O','X','X'}} ;
        int count = 9 ;
        boolean result = FunctionLab3.draw(count) ;
        assertEquals(true,result);
    }
        @Test
    public void testNotDraw1(){
        char[][] table = {{'O','O','X'},{'X','X','O'},{'O','X','-'}} ;
        int count = 8 ;
        boolean result = FunctionLab3.draw(count) ;
        assertEquals(false,result);
    }
}

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

